from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from add_friend.models import Friend
# Create your tests here.

class DashboardUnitTest(TestCase):
	def test_dashboard_url_is_exist(self):
		response = Client().get('/dashboard/')
		self.assertEqual(response.status_code, 200)

	def test_dashboard_using_index_func(self):
		found = resolve('/dashboard/')
		self.assertEqual(found.func, index)

	def test_number_of_friend_on_dashboard_is_right(self):
		new_friend = Friend.objects.create(name='test', url='http://test.com')
		number_of_friend = str(Friend.objects.all().count())
		response= Client().get('/dashboard/')
		html_response = response.content.decode('utf8')
		self.assertIn(number_of_friend, html_response)

	# def test_dashboard_is_showing_latest_post(self):
