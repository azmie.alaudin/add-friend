from django.shortcuts import render
from django.http import HttpResponseRedirect
from add_friend.forms import Add_Form
from add_friend.models import Friend

response = {}
# Create your views here.
def index(request):
	number_of_friend = Friend.objects.all().count()
	response['total'] = number_of_friend
	html = 'dashboard.html'
	return render(request, html, response)